# Honeycomb

This module provides Drupal integration with [Honeycomb](https://www.honeycomb.io/).

Honeycomb sends raw event data from Drupal including events, traces and logs.

Manage Drupal sites logs from a centralized Honeycomb account.

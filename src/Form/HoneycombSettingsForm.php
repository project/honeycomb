<?php

namespace Drupal\honeycomb\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class HoneycombSettingsForm extends ConfigFormBase
{

  /**
   * @var LoggerInterface
   */
  private $logger;

  /**
   * SalutationForm constructor.
   * @param ConfigFactoryInterface $config_factory
   * @param LoggerInterface $logger
   */
  public function __construct(ConfigFactoryInterface $config_factory, LoggerInterface $logger) {
    parent::__construct($config_factory);
    $this->logger = $logger;
  }

  protected function getEditableConfigNames()
  {
    return ['honeycomb.settings'];
  }

  public function getFormId()
  {
    return 'honeycomb_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $config = $this->config('honeycomb.settings');
    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#default_value' => $config->get('api_key'),
    ];

    $levels = array_combine(RfcLogLevel::getLevels(), RfcLogLevel::getLevels());
    $form['log_level'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Log level'),
      '#default_value' => $config->get('log_level'),
      '#description' => $this->t('Select the log level messages to send to Honeycomb.'),
      '#options' => $levels
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $this->config('honeycomb.settings')
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('log_level', $form_state->getValue('log_level'))
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('logger.honeycomb')
    );
  }
}

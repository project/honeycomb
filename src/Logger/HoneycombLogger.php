<?php

namespace Drupal\honeycomb\Logger;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LogMessageParserInterface;
use Drupal\Core\Logger\RfcLoggerTrait;
use Drupal\Core\Logger\RfcLogLevel;
use Psr\Log\LoggerInterface;

class HoneycombLogger implements LoggerInterface
{
  use RfcLoggerTrait;

  /**
   * @var ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * @var LogMessageParserInterface
   */
  private $parser;

  /**
   * MailLogger constructor.
   * @param ConfigFactoryInterface $configFactory
   * @param LogMessageParserInterface $parser
   */
  public function __construct(ConfigFactoryInterface $configFactory, LogMessageParserInterface $parser)
  {
    $this->configFactory = $configFactory;
    $this->parser = $parser;
  }

  /**
   * {@inheritdoc}
   */
  public function log($level, $message, array $context = [])
  {
    $honeycomb_settings = $this->configFactory->get('honeycomb.settings');
    $api_key = $honeycomb_settings->get('api_key');
    $levels = $honeycomb_settings->get('log_level');
    $level_name = RfcLogLevel::getLevels()[$level];

    if (!empty($api_key) && in_array($level_name, array_values($levels))) {

      $message_placeholders = $this->parser->parseMessagePlaceholders($message, $context);
      $message = empty($message_placeholders) ? $message : strtr($message, $message_placeholders);

      $event = [
        'site' => $this->configFactory->get('system.site')->get('name'),
        'level' => $level,
        'message' => $message
      ];
      $this->send_hc_event('drupal', $event, $api_key);
    }
  }

  private function send_hc_event($dataset, $event, $api_key)
  {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://api.honeycomb.io/1/events/' . $dataset);

    $headers[] = 'X-Honeycomb-Team: ' . $api_key;
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($event));
    $result = curl_exec($ch);
  }

}
